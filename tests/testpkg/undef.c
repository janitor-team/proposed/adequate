extern void this_symbol_might_be_undefined(void);

int main(int argc, char **argv)
{
	this_symbol_might_be_undefined();
	return 0;
}
